import asyncHandler from '../middleware/async';
import Reading from '../models/reading';
import Sensor from '../models/sensor';
import ErrorResponse from '../util/errorResponse';

export const addReading = asyncHandler(async (req, res, next) => {
  const { sensorId } = req.params;

  console.log('Creating reading at ', new Date());

  const sensor = await Sensor.findById(sensorId);

  if (!sensor) {
    return next(new ErrorResponse(404, 'Sensor not found'));
  }

  req.body.sensor = sensorId;

  const reading = await Reading.create(req.body);

  res.json({
    success: true,
    data: reading,
  });
});

export const getReadings = asyncHandler(async (req, res) => {
  const { sensorId } = req.params;
  const { from, to } = req.query;

  let dateQuery = {};

  if (from) {
    dateQuery = { date: { ...dateQuery.date, $gte: new Date(from) } };
  }

  if (to) {
    dateQuery = { date: { ...dateQuery.date, $lt: new Date(to) } };
  }

  const query = { sensor: sensorId, ...dateQuery };

  const readings = await Reading.find(query);

  res.json({
    success: true,
    payload: readings,
  });
});

export const getReading = asyncHandler(async (req, res, next) => {
  const reading = await Reading.findById(req.params.id);

  if (!reading) {
    return next(new ErrorResponse(404, 'Reading not found'));
  }

  return res.json({ success: true, payload: reading });
});

export const updateReading = asyncHandler(async (req, res) => res.status(501));

export const deleteReading = asyncHandler(async (req, res, next) => {
  const reading = await Reading.findByIdAndDelete(req.params.id);

  console.log(reading);

  // const event = await Event.findById(req.params.id);
  if (!reading) {
    return next(new ErrorResponse(404, 'Reading not found'));
  }


  return res.json({ success: true, payload: reading });
});

export default {
  addReading,
  getReadings,
  getReading,
  updateReading,
  deleteReading,
};

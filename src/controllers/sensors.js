import asyncHandler from '../middleware/async';
import ErrorResponse from '../util/errorResponse';
import Sensor from '../models/sensor';


export const addSensor = asyncHandler(async (req, res) => {
  const sensor = await Sensor.create(req.body);

  res.json({
    success: true,
    payload: sensor,
  });
});

export const getSensors = asyncHandler(async (req, res) => {
  const sensors = await Sensor.find({});

  res.json({
    success: true,
    payload: sensors,
  });
});

export const getSensor = asyncHandler(async (req, res, next) => {
  const sensor = await Sensor.findById(req.params.id).populate('events').exec();

  if (!sensor) {
    return next(new ErrorResponse(404, 'Sensor not found'));
  }

  return res.json({ success: true, payload: sensor });
});

export const updateSensor = asyncHandler(async (req, res) => res.status(501));

export const deleteSensor = asyncHandler(async (req, res, next) => {
  const sensor = await Sensor.findByIdAndDelete(req.params.id);

  // const calendar = await Calendar.findById(req.params.id);
  if (!sensor) {
    return next(new ErrorResponse(404, 'Sensor not found'));
  }

  return res.json({ success: true, payload: sensor });
});

export default {
  addSensor,
  getSensors,
  getSensor,
  updateSensor,
  deleteSensor,
};

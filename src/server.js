/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import express from 'express';
import 'colors';
import cors from 'cors';
import bodyParser from 'body-parser';

import './config/configure';
import sensorRouter from './routers/sensor';
import readingRouter from './routers/reading';
import connectToDB from './config/db';
import errorHandler from './middleware/errorHandler';

const { MONGO_URI, PORT, ENVIRONMENT } = process.env;

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.json({ message: 'Hello world!' });
});

app.use('/api/v1/sensors', sensorRouter);
app.use('/api/v1/readings', readingRouter);

// Custom error handling
app.use(errorHandler);

let server;

connectToDB(MONGO_URI).then((connection) => {
  console.log(`Connected to DB: ${connection.connection.host}`.green);

  // Start the server
  server = app.listen(PORT, () => {
    console.log(`API running in mode ${ENVIRONMENT} at port ${PORT}`.green);
  });
});

// Handle unhandled promise rejections
process.on('unhandledRejection', (err) => {
  console.log(`Error: ${err.message}`.red);
  // close server and exit process
  if (server) {
    server.close(() => {
      process.exit(1);
    });
  }
});

class ErrorResponse extends Error {
  constructor(statusCode, description = null) {
    super(description);
    this.statusCode = statusCode;
    this.description = description;
  }
}

export default ErrorResponse;

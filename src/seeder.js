/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import fs from 'fs';
import colors from 'colors';
import path from 'path';
import './config/configure';
import connectDB from './config/db';
import Sensor from './models/sensor';
import Reading from './models/reading';

console.log(__dirname);

// Reads a file located in _data dir and returns a promise of json parsed items
const loadData = async (fileName) => new Promise((resolve, reject) => {
  fs.readFile(path.join(__dirname, '../_data', fileName), (err, json) => {
    if (err) reject(err);

    try {
      const items = JSON.parse(json);
      console.log(`${items.length} items loaded !`.green);
      resolve(items);
    } catch (error) {
      reject(error);
    }
  });
});

// Seed DB
const importData = async () => {
  try {
    console.log('Importing data...'.yellow);

    // Seed sensors
    console.log('Seeding sensors...'.yellow);
    const sensors = await loadData('sensors.json');
    console.log('Creating sensors...'.yellow);
    await Sensor.create(sensors);
    console.log(`${sensors.length} sensors have been created successfully!`.green);

    // Seed readings
    console.log('Seeding readings...'.yellow);
    const readings = await loadData('readings.json');
    console.log('Creating readings...'.yellow);
    await Reading.create(readings);
    console.log(`${readings.length} readings have been created successfully!`.green);

    console.log('Data imported!'.green.inverse);
    process.exit(0);
  } catch (error) {
    console.error(error.message.red.inverse);
    process.exit(1);
  }
};

// Erase DB
const deleteData = async () => {
  try {
    console.log('Erasing Readings...'.yellow);
    await Reading.deleteMany();
    console.log('Done!'.green);

    console.log('Erasing Sensors...'.yellow);
    await Sensor.deleteMany();
    console.log('Done!'.green);

    console.log('Data deleted!'.green.inverse);
    process.exit(0);
  } catch (error) {
    console.error(error.message.red.inverse);
    process.exit(1);
  }
};

console.log('Initializing...'.yellow);

connectDB(process.env.MONGO_URI).then((connection) => {
  console.log(`Connected to DB: ${connection.connection.host}`.green);
  switch (process.argv[2]) {
    case '--i':
      importData();
      break;
    case '--d':
      deleteData();
      break;
    default:
      console.log(`Invalid argument {${process.argv[2]}} ( --import | --delete )`);
  }
});

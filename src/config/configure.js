import dotenv from 'dotenv';
import 'colors';

const path = `${__dirname}/../../.env`;

console.log(`Configuring with env file ${path}`.yellow);

dotenv.config({ path });

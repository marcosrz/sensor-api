import mongoose from 'mongoose';

const connectDB = (uri) => mongoose.connect(uri, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
});

export default connectDB;

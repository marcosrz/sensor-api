import express from 'express';
import {
  addReading, deleteReading, getReading, getReadings, updateReading,
} from '../controllers/readings';

const router = express.Router({ mergeParams: true });

router.route('/')
  .get(getReadings)
  .post(addReading);

router.route('/:id')
  .get(getReading)
  .put(updateReading)
  .delete(deleteReading);

export default router;

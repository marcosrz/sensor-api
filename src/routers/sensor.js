import express from 'express';
import readingsRouter from './reading';

import {
  addSensor, deleteSensor, getSensor, getSensors, updateSensor,
} from '../controllers/sensors';

const router = express.Router();

router.use('/:sensorId/readings', readingsRouter);

router.route('/')
  .get(getSensors)
  .post(addSensor);

router.route('/:id')
  .get(getSensor)
  .put(updateSensor)
  .delete(deleteSensor);

export default router;

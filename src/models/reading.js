import mongoose from 'mongoose';

const ReadingSchema = new mongoose.Schema({
  unit: {
    type: String,
    trim: true,
    required: [true, 'Missing unit'],
  },
  value: {
    type: Number,
    default: 0,
    required: [true, 'Missing value'],
  },
  raw: {
    type: String,
    default: '',
    required: [true, 'Missing raw'],
  },
  sensor: {
    type: mongoose.Schema.ObjectId,
    ref: 'Sensor',
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model('Reading', ReadingSchema);

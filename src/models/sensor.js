/* eslint-disable no-underscore-dangle */
import mongoose from 'mongoose';

const SensorScheme = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: [true, 'Missing title'],
  },
  description: {
    type: String,
    required: [true, 'Missing color'],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },

}, {
  toJSON: { virtuals: true },
  toObject: { virtuals: true },
});

SensorScheme.virtual('readings', {
  ref: 'Reading',
  localField: '_id',
  foreignField: 'sensor',
  justOne: false,
});

// Cascade delete readings when a sensor is deleted
SensorScheme.pre('remove', async function (next) {
  console.log(`Readings being removed from sensor ${this._id}`);
  await this.model('Reading').deleteMany({ sensor: this._id });
  next();
});


export default mongoose.model('Sensor', SensorScheme);

import ErrorResponse from '../util/errorResponse';

/* eslint-disable no-console */
export default (err, req, res, next) => {
  let error = { ...err };

  console.log('ERROR', err);

  // Invalid _id Format
  if (error.name === 'CastError') {
    const message = 'Invalid _id format';
    error = new ErrorResponse(404, message);
  }

  // Duplicated field
  if (error.code === 11000) {
    const message = 'Duplicated property validation failed';
    error = new ErrorResponse(400, message);
  }

  // Validation error
  if (error.name === 'ValidationError') {
    const errors = Object.values(err.errors);
    const message = errors.map((e) => e.message).join('. ');
    error = new ErrorResponse(400, message);
  }

  if (error.statusCode && !error.description) {
    switch (error.statusCode) {
      case 400:
        error.description = 'Bad request';
        break;
      case 401:
        error.description = 'Unauthorized';
        break;
      case 403:
        error.description = 'Forbidden';
        break;
      case 404:
        error.description = 'Not Found';
        break;
      case 500:
      default:
        error.description = 'Internal Server Error';
    }
  }

  res.status(error.statusCode || 500).json({ success: false, error: error.description });
};

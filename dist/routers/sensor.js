"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _reading = _interopRequireDefault(require("./reading"));

var _sensors = require("../controllers/sensors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

router.use('/:sensorId/readings', _reading.default);
router.route('/').get(_sensors.getSensors).post(_sensors.addSensor);
router.route('/:id').get(_sensors.getSensor).put(_sensors.updateSensor).delete(_sensors.deleteSensor);
var _default = router;
exports.default = _default;
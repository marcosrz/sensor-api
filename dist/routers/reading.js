"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _readings = require("../controllers/readings");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router({
  mergeParams: true
});

router.route('/').get(_readings.getReadings).post(_readings.addReading);
router.route('/:id').get(_readings.getReading).put(_readings.updateReading).delete(_readings.deleteReading);
var _default = router;
exports.default = _default;
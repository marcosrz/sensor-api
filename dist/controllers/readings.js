"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.deleteReading = exports.updateReading = exports.getReading = exports.getReadings = exports.addReading = void 0;

var _async = _interopRequireDefault(require("../middleware/async"));

var _reading = _interopRequireDefault(require("../models/reading"));

var _sensor = _interopRequireDefault(require("../models/sensor"));

var _errorResponse = _interopRequireDefault(require("../util/errorResponse"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const addReading = (0, _async.default)(async (req, res, next) => {
  const {
    sensorId
  } = req.params;
  console.log('Creating reading at ', new Date());
  const sensor = await _sensor.default.findById(sensorId);

  if (!sensor) {
    return next(new _errorResponse.default(404, 'Sensor not found'));
  }

  req.body.sensor = sensorId;
  const reading = await _reading.default.create(req.body);
  res.json({
    success: true,
    data: reading
  });
});
exports.addReading = addReading;
const getReadings = (0, _async.default)(async (req, res) => {
  const {
    sensorId
  } = req.params;
  console.log(req.params);
  const readings = await _reading.default.find({
    sensor: sensorId
  });
  res.json({
    success: true,
    payload: readings
  });
});
exports.getReadings = getReadings;
const getReading = (0, _async.default)(async (req, res, next) => {
  const reading = await _reading.default.findById(req.params.id);

  if (!reading) {
    return next(new _errorResponse.default(404, 'Reading not found'));
  }

  return res.json({
    success: true,
    payload: reading
  });
});
exports.getReading = getReading;
const updateReading = (0, _async.default)(async (req, res) => res.status(501));
exports.updateReading = updateReading;
const deleteReading = (0, _async.default)(async (req, res, next) => {
  const reading = await _reading.default.findByIdAndDelete(req.params.id);
  console.log(reading); // const event = await Event.findById(req.params.id);

  if (!reading) {
    return next(new _errorResponse.default(404, 'Reading not found'));
  }

  return res.json({
    success: true,
    payload: reading
  });
});
exports.deleteReading = deleteReading;
var _default = {
  addReading,
  getReadings,
  getReading,
  updateReading,
  deleteReading
};
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.deleteSensor = exports.updateSensor = exports.getSensor = exports.getSensors = exports.addSensor = void 0;

var _async = _interopRequireDefault(require("../middleware/async"));

var _errorResponse = _interopRequireDefault(require("../util/errorResponse"));

var _sensor = _interopRequireDefault(require("../models/sensor"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const addSensor = (0, _async.default)(async (req, res) => {
  const sensor = await _sensor.default.create(req.body);
  res.json({
    success: true,
    payload: sensor
  });
});
exports.addSensor = addSensor;
const getSensors = (0, _async.default)(async (req, res) => {
  const sensors = await _sensor.default.find({});
  res.json({
    success: true,
    payload: sensors
  });
});
exports.getSensors = getSensors;
const getSensor = (0, _async.default)(async (req, res, next) => {
  const sensor = await _sensor.default.findById(req.params.id).populate('events').exec();

  if (!sensor) {
    return next(new _errorResponse.default(404, 'Sensor not found'));
  }

  return res.json({
    success: true,
    payload: sensor
  });
});
exports.getSensor = getSensor;
const updateSensor = (0, _async.default)(async (req, res) => res.status(501));
exports.updateSensor = updateSensor;
const deleteSensor = (0, _async.default)(async (req, res, next) => {
  const sensor = await _sensor.default.findByIdAndDelete(req.params.id); // const calendar = await Calendar.findById(req.params.id);

  if (!sensor) {
    return next(new _errorResponse.default(404, 'Sensor not found'));
  }

  return res.json({
    success: true,
    payload: sensor
  });
});
exports.deleteSensor = deleteSensor;
var _default = {
  addSensor,
  getSensors,
  getSensor,
  updateSensor,
  deleteSensor
};
exports.default = _default;
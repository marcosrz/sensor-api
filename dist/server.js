"use strict";

var _express = _interopRequireDefault(require("express"));

var _colors = _interopRequireDefault(require("colors"));

var _cors = _interopRequireDefault(require("cors"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

require("./config/configure");

var _sensor = _interopRequireDefault(require("./routers/sensor"));

var _reading = _interopRequireDefault(require("./routers/reading"));

var _db = _interopRequireDefault(require("./config/db"));

var _errorHandler = _interopRequireDefault(require("./middleware/errorHandler"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable no-unused-vars */

/* eslint-disable no-console */
const environment = process.env.NODE_ENV;
const PORT = process.argv[2] || 3001;
const app = (0, _express.default)();
app.use((0, _cors.default)());
app.use(_bodyParser.default.json());
app.get('/', (req, res) => {
  res.json({
    message: 'Hello world!'
  });
});
app.use('/api/v1/sensors', _sensor.default);
app.use('/api/v1/readings', _reading.default); // Custom error handling

app.use(_errorHandler.default);
let server;
console.log(process.env.MONGO_URI);
(0, _db.default)(process.env.MONGO_URI).then(connection => {
  console.log(`Connected to DB: ${connection.connection.host}`.green); // Start the server

  server = app.listen(PORT, () => {
    console.log(`API running in mode ${environment} at port ${PORT}`.green);
  });
}); // Handle unhandled promise rejections

process.on('unhandledRejection', err => {
  console.log(`Error: ${err.message}`.red); // close server and exit process

  if (server) {
    server.close(() => {
      process.exit(1);
    });
  }
});
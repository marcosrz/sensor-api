"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

class ErrorResponse extends Error {
  constructor(statusCode, description = null) {
    super(description);
    this.statusCode = statusCode;
    this.description = description;
  }

}

var _default = ErrorResponse;
exports.default = _default;
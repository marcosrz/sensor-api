"use strict";

var _dotenv = _interopRequireDefault(require("dotenv"));

require("colors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const path = `${__dirname}/../../.env`;
console.log(`Configuring with env file ${path}`.yellow);

_dotenv.default.config({
  path
});
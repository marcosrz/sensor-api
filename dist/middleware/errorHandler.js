"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _ErrorResponse = _interopRequireDefault(require("../util/ErrorResponse"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable no-console */
var _default = (err, req, res, next) => {
  let error = { ...err
  };
  console.log('ERROR', err); // Invalid _id Format

  if (error.name === 'CastError') {
    const message = 'Invalid _id format';
    error = new _ErrorResponse.default(404, message);
  } // Duplicated field


  if (error.code === 11000) {
    const message = 'Duplicated property validation failed';
    error = new _ErrorResponse.default(400, message);
  } // Validation error


  if (error.name === 'ValidationError') {
    const errors = Object.values(err.errors);
    const message = errors.map(e => e.message).join('. ');
    error = new _ErrorResponse.default(400, message);
  }

  if (error.statusCode && !error.description) {
    switch (error.statusCode) {
      case 400:
        error.description = 'Bad request';
        break;

      case 401:
        error.description = 'Unauthorized';
        break;

      case 403:
        error.description = 'Forbidden';
        break;

      case 404:
        error.description = 'Not Found';
        break;

      case 500:
      default:
        error.description = 'Internal Server Error';
    }
  }

  res.status(error.statusCode || 500).json({
    success: false,
    error: error.description
  });
};

exports.default = _default;
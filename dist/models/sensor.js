"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable no-underscore-dangle */
const SensorScheme = new _mongoose.default.Schema({
  name: {
    type: String,
    trim: true,
    required: [true, 'Missing title']
  },
  description: {
    type: String,
    required: [true, 'Missing color']
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
}, {
  toJSON: {
    virtuals: true
  },
  toObject: {
    virtuals: true
  }
});
SensorScheme.virtual('readings', {
  ref: 'Reading',
  localField: '_id',
  foreignField: 'sensor',
  justOne: false
}); // Cascade delete readings when a sensor is deleted

SensorScheme.pre('remove', async function (next) {
  console.log(`Readings being removed from sensor ${this._id}`);
  await this.model('Reading').deleteMany({
    sensor: this._id
  });
  next();
});

var _default = _mongoose.default.model('Sensor', SensorScheme);

exports.default = _default;
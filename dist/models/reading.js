"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ReadingSchema = new _mongoose.default.Schema({
  unit: {
    type: String,
    trim: true,
    required: [true, 'Missing unit']
  },
  value: {
    type: Number,
    default: 0,
    required: [true, 'Missing value']
  },
  raw: {
    type: String,
    default: '',
    required: [true, 'Missing raw']
  },
  sensor: {
    type: _mongoose.default.Schema.ObjectId,
    ref: 'Sensor',
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

var _default = _mongoose.default.model('Reading', ReadingSchema);

exports.default = _default;